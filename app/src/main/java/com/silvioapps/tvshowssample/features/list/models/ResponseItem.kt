package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.silvioapps.tvshowssample.features.list.models.Show
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class ResponseItem(
	@PrimaryKey(autoGenerate = false)
	@field:SerializedName("id")
	var id: Int? = null,

	@Ignore
	var showLoading: Boolean = false,

	@field:SerializedName("score")
	var score: Double? = null,

	@Embedded(prefix = "show_")
	@field:SerializedName("show")
	var show: Show? = null
): Serializable