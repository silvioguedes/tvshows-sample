package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.constants.Constants
import com.silvioapps.tvshowssample.features.list.services.ListService
import com.silvioapps.tvshowssample.features.shared.services.ServiceGenerator
import dagger.Module
import dagger.Provides

@Module
open class ApiModule{

    @Provides
    open fun providesListService(): ListService {
        return ServiceGenerator.createService(Constants.API_BASE_URL, Constants.API_TIMEOUT, true, ListService::class.java)
    }
}