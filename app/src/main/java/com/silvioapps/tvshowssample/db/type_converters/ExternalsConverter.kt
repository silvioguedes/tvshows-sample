package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Externals

class ExternalsConverter{

    @TypeConverter
    fun from(value: Externals): String{
        val gson = Gson()
        val type = object: TypeToken<Externals>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Externals{
        val gson = Gson()
        val type = object: TypeToken<Externals>(){}.type
        return gson.fromJson(value, type)
    }
}