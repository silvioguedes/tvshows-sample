package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Network

class NetworkConverter{

    @TypeConverter
    fun from(value: Network): String{
        val gson = Gson()
        val type = object: TypeToken<Network>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Network{
        val gson = Gson()
        val type = object: TypeToken<Network>(){}.type
        return gson.fromJson(value, type)
    }
}