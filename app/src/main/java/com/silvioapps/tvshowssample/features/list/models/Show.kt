package com.silvioapps.tvshowssample.features.list.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Show(
	@field:SerializedName("id")
	var id: Int? = null,

	@field:SerializedName("summary")
	var summary: String? = null,

	@field:SerializedName("image")
	var image: Image? = null,

	@field:SerializedName("_links")
	var links: Links? = null,

	@field:SerializedName("premiered")
	var premiered: String? = null,

	@field:SerializedName("rating")
	var rating: Rating? = null,

	@field:SerializedName("runtime")
	var runtime: Int? = null,

	@field:SerializedName("weight")
	var weight: Int? = null,

	@field:SerializedName("language")
	var language: String? = null,

	@field:SerializedName("type")
	var type: String? = null,

	@field:SerializedName("url")
	var url: String? = null,

	@field:SerializedName("officialSite")
	var officialSite: String? = null,

	@field:SerializedName("network")
	var network: Network? = null,

	@field:SerializedName("schedule")
	var schedule: Schedule? = null,

	//@field:SerializedName("webChannel")
	//var webChannel: Any? = null,

	@field:SerializedName("genres")
	var genres: List<String>? = null,

	@field:SerializedName("name")
	var name: String? = null,

	@field:SerializedName("externals")
	var externals: Externals? = null,

	@field:SerializedName("updated")
	var updated: Int? = null,

	@field:SerializedName("status")
	var status: String? = null
): Serializable