package com.silvioapps.tvshowssample.features.list.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.silvioapps.tvshowssample.features.shared.arc.Resource
import com.silvioapps.tvshowssample.features.list.repositories.ListRepository
import com.silvioapps.tvshowssample.features.list.models.ResponseItem
import javax.inject.Inject;

class ListViewModel @Inject constructor(private var repository: ListRepository): ViewModel() {

    fun setResponseItemList(value: List<ResponseItem>){
        repository.setResponseItemList(value)
    }

    fun getLiveDataResorceResponseItemList(search: String): LiveData<Resource<List<ResponseItem>>> {
        return repository.getLiveDataResorceResponseItemList(search)
    }
}