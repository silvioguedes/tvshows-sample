package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Country

class CountryConverter{

    @TypeConverter
    fun from(value: Country): String{
        val gson = Gson()
        val type = object: TypeToken<Country>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Country{
        val gson = Gson()
        val type = object: TypeToken<Country>(){}.type
        return gson.fromJson(value, type)
    }
}