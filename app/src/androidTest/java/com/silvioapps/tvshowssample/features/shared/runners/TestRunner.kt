package com.silvioapps.tvshowssample.features.shared.runners

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.silvioapps.tvshowssample.di.applications.AppInstrumentedTest

class TestRunner: AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, AppInstrumentedTest::class.java.name, context)
    }
}