package com.silvioapps.tvshowssample.features.shared.arc

enum class Status {
    UNKNOWN_CODE,
    SUCCESS,
    ERROR,
    LOADING
}