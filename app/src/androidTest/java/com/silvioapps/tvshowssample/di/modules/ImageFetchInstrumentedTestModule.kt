package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.tvshowssample.features.list.implementations.ListImageFetchListener
import com.silvioapps.tvshowssample.features.details.implementations.DetailsImageFetchListenerInstrumentedTestImpl
import com.silvioapps.tvshowssample.features.list.implementations.ListImageFetchListenerInstrumentedTestImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ImageFetchInstrumentedTestModule: ImageFetchModule() {

    @Provides
    @Singleton
    override fun providesListImageFetchListener(): ListImageFetchListener {
        return ListImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }

    @Provides
    @Singleton
    override fun providesDetailsImageFetchListener(): DetailsImageFetchListener {
        return DetailsImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }
}