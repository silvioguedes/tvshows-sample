package com.silvioapps.tvshowssample.db.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.silvioapps.tvshowssample.features.list.daos.ListDao
import com.silvioapps.tvshowssample.db.type_converters.*
import com.silvioapps.tvshowssample.features.list.models.*

@TypeConverters(CountryConverter::class, ExternalsConverter::class, ImageConverter::class, IntConverter::class,
    LinksConverter::class, NetworkConverter::class, RatingConverter::class, ResponseItemConverter::class,
    ScheduleConverter::class, SelfConverter::class, ShowConverter::class, StringConverter::class)
@Database(entities = [ResponseItem::class],
    version = 2, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun listDao(): ListDao
}