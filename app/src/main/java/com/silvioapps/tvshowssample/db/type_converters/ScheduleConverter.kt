package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Schedule

class ScheduleConverter{

    @TypeConverter
    fun from(value: Schedule): String{
        val gson = Gson()
        val type = object: TypeToken<Schedule>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Schedule{
        val gson = Gson()
        val type = object: TypeToken<Schedule>(){}.type
        return gson.fromJson(value, type)
    }
}