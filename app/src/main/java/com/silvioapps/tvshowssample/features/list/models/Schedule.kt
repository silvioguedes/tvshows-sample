package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Schedule(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("days")
	var days: List<String>? = null,

	@field:SerializedName("time")
	var time: String? = null
): Serializable