package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.features.details.fragments.DetailsFragment
import com.silvioapps.tvshowssample.features.list.fragments.MainFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailsFragment(): DetailsFragment
}
