package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Self(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("href")
	var href: String? = null
): Serializable