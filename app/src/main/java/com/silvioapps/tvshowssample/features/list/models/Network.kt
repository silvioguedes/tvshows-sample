package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Network(
	@PrimaryKey(autoGenerate = true)
	@field:SerializedName("id")
	var id: Int? = null,

	@field:SerializedName("country")
	var country: Country? = null,

	@field:SerializedName("name")
	var name: String? = null
): Serializable