package com.silvioapps.tvshowssample.features.list.fragments

import android.annotation.SuppressLint
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.recyclerview.widget.DefaultItemAnimator
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.silvioapps.tvshowssample.R
import com.silvioapps.tvshowssample.databinding.FragmentMainBinding
import com.silvioapps.tvshowssample.features.list.adapters.MainListAdapter
import com.silvioapps.tvshowssample.features.list.models.ResponseItem
import com.silvioapps.tvshowssample.features.shared.fragments.CustomFragment
import com.silvioapps.tvshowssample.features.shared.listeners.FetcherListener
import dagger.android.support.AndroidSupportInjection
import java.io.Serializable
import javax.inject.Inject
import com.silvioapps.tvshowssample.features.list.view_models.ListViewModel
import com.silvioapps.tvshowssample.features.shared.arc.Resource
import com.silvioapps.tvshowssample.features.shared.arc.Status
import com.silvioapps.tvshowssample.features.shared.utils.Utils
import com.silvioapps.tvshowssample.statics.Statics
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainFragment @Inject constructor(): CustomFragment() {
    @Inject lateinit var mainListAdapter: MainListAdapter
    private lateinit var fragmentMainBinding: FragmentMainBinding
    @Inject lateinit var context_: Context
    @Inject lateinit var linearLayoutManager: LinearLayoutManager
    @Inject lateinit var defaultItemAnimator: DefaultItemAnimator
    @Inject lateinit var fetcherListener: FetcherListener
    private var searchViewExpanded = false
    private var searchViewQuery: String? = null
    private var isSearching = false
    private var page = 1
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    val listViewModel: ListViewModel by viewModels{
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View?{
        fragmentMainBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_main, viewGroup, false)

        fragmentMainBinding.recyclerView.apply{
            layoutManager = linearLayoutManager
            itemAnimator = defaultItemAnimator
            setHasFixedSize(true)
            adapter = mainListAdapter
        }

        if(bundle != null){
            @Suppress("UNCHECKED_CAST")
            setResponseItemList(bundle.getSerializable("responseItemList") as List<ResponseItem>)
            searchViewExpanded = bundle.getBoolean("searchViewExpanded")
            searchViewQuery = bundle.getString("searchViewQuery")
            page = bundle.getInt("page")
            isSearching = bundle.getBoolean("isSearching")
        }
        else{
            page = 1

            initList()
        }

        return fragmentMainBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onSaveInstanceState(outState : Bundle) {
        outState.putSerializable("responseItemList", mainListAdapter.list as Serializable)
        outState.putBoolean("searchViewExpanded", searchViewExpanded)
        outState.putString("searchViewQuery", searchViewQuery)
        outState.putInt("page", page)
        outState.putBoolean("isSearching", isSearching)
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater)

        menuInflater.inflate(R.menu.menu_main, menu)

        val searchView = menu.findItem(R.id.action_search).getActionView() as SearchView
        if(searchViewExpanded){
            searchView.setIconifiedByDefault(false)
            searchView.setQuery(searchViewQuery, false)
        }
        searchView.setMaxWidth(Integer.MAX_VALUE)
        searchView.queryHint = getString(R.string.search_view_query_hint)
        Utils.setQueryHintColor(searchView, R.color.colorText)
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                page = 1

                initList()

                search("%"+query+"%" )

                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                searchViewQuery = query
                searchViewExpanded = false
                if(query.length > 0) {
                    searchViewExpanded = true
                }
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == R.id.action_search) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun search(search: String) {
        isSearching = true
        fetcherListener.beginFetching()
        fragmentMainBinding.progressBar.setVisibility(View.VISIBLE)

        listViewModel.getLiveDataResorceResponseItemList(search)
            .observe(this, object: Observer<Resource<List<ResponseItem>>> {
                override fun onChanged(t: Resource<List<ResponseItem>>?) {
                    if (t?.status == Status.SUCCESS) {
                        addResponseItemList(t.data!!)

                        page++

                        fetcherListener.doneFetching()
                    }
                    else if (t?.status == Status.ERROR) {
                        fragmentMainBinding.progressBar.setVisibility(View.GONE)

                        Log.i("TAG***","ERROR "+t.message)

                        Toast.makeText(context_, getString(R.string.list_error), Toast.LENGTH_LONG).show()
                        fetcherListener.doneFetching()
                    }
                    else if (t?.status == Status.LOADING) {
                        fetcherListener.beginFetching()
                    }
                }
            })
    }

    protected fun addResponseItemList(values : List<ResponseItem>){
        val startRange = mainListAdapter.list.size
        val endRange = values.size
        mainListAdapter.list.addAll(values)

        mainListAdapter.notifyItemRangeInserted(startRange, endRange)

        fragmentMainBinding.progressBar.setVisibility(View.GONE)

        Statics.loadMore = false

        GlobalScope.launch {
            withContext(IO){
                listViewModel.setResponseItemList(values)
            }
        }
    }

    protected fun setResponseItemList(values: List<ResponseItem>){
        mainListAdapter.list.clear()
        mainListAdapter.list.addAll(values)
        mainListAdapter.notifyDataSetChanged()

        fragmentMainBinding.progressBar.setVisibility(View.GONE)
    }

    protected fun initList(){
        mainListAdapter.list.clear()
        mainListAdapter.notifyDataSetChanged()
        fragmentMainBinding.recyclerView.scrollToPosition(0)
    }
}
