package com.silvioapps.tvshowssample.features.shared.listeners

interface ViewInflatedListener {
    fun onInflated()
}
