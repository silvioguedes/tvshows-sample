package com.silvioapps.tvshowssample.features.list.services

import androidx.lifecycle.LiveData
import com.silvioapps.tvshowssample.constants.Constants
import com.silvioapps.tvshowssample.features.list.models.ResponseItem
import com.silvioapps.tvshowssample.features.shared.arc.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ListService {
    @GET(Constants.LIST)
    fun getLiveDataApiResponse(@Query("q") query: String): LiveData<ApiResponse<List<ResponseItem>>>
}