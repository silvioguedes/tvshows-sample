package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Links

class LinksConverter{

    @TypeConverter
    fun from(value: Links): String{
        val gson = Gson()
        val type = object: TypeToken<Links>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Links{
        val gson = Gson()
        val type = object: TypeToken<Links>(){}.type
        return gson.fromJson(value, type)
    }
}