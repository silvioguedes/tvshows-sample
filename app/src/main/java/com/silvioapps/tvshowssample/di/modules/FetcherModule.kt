package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.features.list.implementations.ListFetcherListenerImpl
import com.silvioapps.tvshowssample.features.shared.listeners.FetcherListener
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class FetcherModule {

    @Provides
    @Singleton
    open fun providesFetcherListener(): FetcherListener {
        return ListFetcherListenerImpl()
    }
}