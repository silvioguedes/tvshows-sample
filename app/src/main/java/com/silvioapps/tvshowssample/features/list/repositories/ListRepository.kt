package com.silvioapps.tvshowssample.features.list.repositories

import java.util.concurrent.TimeUnit
import androidx.lifecycle.LiveData
import com.silvioapps.tvshowssample.constants.Constants
import com.silvioapps.tvshowssample.features.shared.arc.AppExecutors
import com.silvioapps.tvshowssample.features.shared.arc.NetworkBoundResource
import com.silvioapps.tvshowssample.features.shared.arc.RateLimiter
import com.silvioapps.tvshowssample.features.shared.arc.Resource
import com.silvioapps.tvshowssample.features.list.daos.ListDao
import com.silvioapps.tvshowssample.features.list.models.ResponseItem
import com.silvioapps.tvshowssample.features.list.services.ListService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListRepository @Inject constructor(private val appExecutors: AppExecutors, private val dao: ListDao,
                                         private val listService: ListService
){
    private val rateLimit = RateLimiter<String>(Constants.RATE_TIMEOUT, TimeUnit.MINUTES)

    fun setResponseItemList(value: List<ResponseItem>){
        dao.setResponseItemList(value)
    }

    fun getLiveDataResorceResponseItemList(search: String): LiveData<Resource<List<ResponseItem>>> {
        return object : NetworkBoundResource<List<ResponseItem>, List<ResponseItem>>(appExecutors) {
            override fun saveCallResult(item: List<ResponseItem>) {
                dao.setResponseItemList(item)
            }

            override fun shouldFetch(data: List<ResponseItem>?): Boolean {
                return data == null || rateLimit.shouldFetch(search)
            }

            override fun loadFromDb(): LiveData<List<ResponseItem>> {
                return dao.getLiveDataResponseItemList(search)
            }

            override fun createCall() = listService.getLiveDataApiResponse(search)

            override fun onFetchFailed() {}
            
        }.asLiveData()
    }
}

