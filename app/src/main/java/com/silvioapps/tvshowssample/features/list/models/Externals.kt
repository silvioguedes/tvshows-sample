package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Externals(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("thetvdb")
	var thetvdb: Int? = null,

	@field:SerializedName("imdb")
	var imdb: String? = null,

	@field:SerializedName("tvrage")
	var tvrage: Int? = null
): Serializable