package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Self

class SelfConverter{

    @TypeConverter
    fun from(value: Self): String{
        val gson = Gson()
        val type = object: TypeToken<Self>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Self{
        val gson = Gson()
        val type = object: TypeToken<Self>(){}.type
        return gson.fromJson(value, type)
    }
}