package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Show

class ShowConverter{

    @TypeConverter
    fun from(value: Show): String{
        val gson = Gson()
        val type = object: TypeToken<Show>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Show{
        val gson = Gson()
        val type = object: TypeToken<Show>(){}.type
        return gson.fromJson(value, type)
    }
}