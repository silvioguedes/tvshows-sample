package com.silvioapps.tvshowssample.constants

class Constants {
    companion object {
        const val API_BASE_URL = "http://api.tvmaze.com/"
        //const val THUMBNAIL_BASE_URL = "http://image.tmdb.org/t/p/w185/"
        //const val POSTER_BASE_URL = "http://image.tmdb.org/t/p/original/"
        const val LIST = "search/shows/"
        //const val SEARCH = "search/movie"
        //const val API_KEY = "a57f64f684d129943f0dedc73430ec6a"
        const val API_TIMEOUT : Long = 15
        const val RATE_TIMEOUT : Long = 10
        //const val LIMIT : Int = 30
        //const val DATE_FORMAT = "dd/MM/yyyy"
    }
}
