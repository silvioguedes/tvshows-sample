package com.silvioapps.tvshowssample.features.details.implementations

import com.silvioapps.tvshowssample.features.shared.listeners.ImageFetchListener
import javax.inject.Inject

interface DetailsImageFetchListener: ImageFetchListener

class DetailsImageFetchListenerImpl @Inject constructor(): DetailsImageFetchListener {
    override fun beginFetching(){}
    override fun doneFetching(){}
    override fun isIdle(): Boolean{
        return true
    }
    override fun setPosition(position: Int){}
    override fun getPosition(): Int{
        return 0
    }
}