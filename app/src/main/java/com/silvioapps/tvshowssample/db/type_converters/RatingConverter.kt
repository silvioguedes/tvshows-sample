package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Rating

class RatingConverter{

    @TypeConverter
    fun from(value: Rating): String{
        val gson = Gson()
        val type = object: TypeToken<Rating>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): Rating{
        val gson = Gson()
        val type = object: TypeToken<Rating>(){}.type
        return gson.fromJson(value, type)
    }
}