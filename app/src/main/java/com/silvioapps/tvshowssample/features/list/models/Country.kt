package com.silvioapps.tvshowssample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class Country(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("code")
	var code: String? = null,

	@field:SerializedName("timezone")
	var timezone: String? = null,

	@field:SerializedName("name")
	var name: String? = null
): Serializable