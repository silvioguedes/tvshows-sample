package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.tvshowssample.features.details.implementations.DetailsImageFetchListenerImpl
import com.silvioapps.tvshowssample.features.list.implementations.ListImageFetchListener
import com.silvioapps.tvshowssample.features.list.implementations.ListImageFetchListenerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class ImageFetchModule {

    @Provides
    @Singleton
    open fun providesListImageFetchListener(): ListImageFetchListener {
        return ListImageFetchListenerImpl()
    }

    @Provides
    @Singleton
    open fun providesDetailsImageFetchListener(): DetailsImageFetchListener {
        return DetailsImageFetchListenerImpl()
    }
}