package com.silvioapps.tvshowssample.di.modules

import com.silvioapps.tvshowssample.features.details.activities.DetailsActivity
import com.silvioapps.tvshowssample.features.list.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailsActivity(): DetailsActivity
}
