package com.silvioapps.tvshowssample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.tvshowssample.features.list.models.Image

class ImageConverter{

    @TypeConverter
    fun from(value: Image?): String{
        val gson = Gson()
        val type = object: TypeToken<Image>(){}.type
        return gson.toJson(value!!, type)
    }

    @TypeConverter
    fun to(value: String): Image{
        val gson = Gson()
        val type = object: TypeToken<Image>(){}.type
        return gson.fromJson(value, type)
    }
}