package com.silvioapps.tvshowssample.features.list.implementations

import com.silvioapps.tvshowssample.features.shared.listeners.ImageFetchListener
import javax.inject.Inject

interface ListImageFetchListener: ImageFetchListener

class ListImageFetchListenerImpl @Inject constructor(): ListImageFetchListener {
    override fun beginFetching(){}
    override fun doneFetching(){}
    override fun isIdle(): Boolean{
        return true
    }
    override fun setPosition(position: Int){}
    override fun getPosition(): Int{
        return 0
    }
}